package main

import (
	"fmt"
	"net"
	"net/textproto"
	"os"

	"tonyfinn.com/gochat/shared"
)

type ClientConnection struct {
	conn   *textproto.Conn
	user   shared.User
	output chan shared.Message
	events ServerEvents
}

type ServerEvents struct {
	newConnections         chan net.Conn
	establishedConnections chan ClientConnection
	disconnectedUsers      chan shared.UserId
	messages               chan shared.Message
}

type ChatServer struct {
	connectedUsers map[shared.UserId]ClientConnection
	events         ServerEvents
	nextUserId     shared.UserId
	user           shared.User
}

func (cc *ClientConnection) run() {
	messages := make(chan string)
	shouldClose := make(chan bool)

	go (func() {
		for {
			input, err := cc.conn.ReadLine()
			if err != nil {
				fmt.Println("Client exited", cc.user.Id)
				break
			}
			if input == "QUIT" {
				shouldClose <- true
				break
			} else {
				messages <- input
			}
		}
	})()

	for {
		select {
		case recvd := <-messages:
			cc.events.messages <- shared.Message{
				UserId:      cc.user.Id,
				DisplayName: cc.user.DisplayName,
				Content:     recvd,
			}
		case output := <-cc.output:
			cc.conn.PrintfLine("[%s]: %s", output.DisplayName, output.Content)
		case <-shouldClose:
			cc.conn.Close()
			cc.events.disconnectedUsers <- cc.user.Id
			break
		}
	}
}

func handleConnect(c net.Conn, uId shared.UserId, events ServerEvents) {
	conn := textproto.NewConn(c)
	line, err := conn.ReadLine()
	if err == nil {
		user := shared.User{
			Id:          uId,
			DisplayName: line,
		}
		events.establishedConnections <- ClientConnection{
			conn:   conn,
			user:   user,
			output: make(chan shared.Message, 10),
			events: events,
		}
	}
}

func sendToAll(serv *ChatServer, message shared.Message) {
	for _, cc := range serv.connectedUsers {
		cc.output <- message
	}
}

func notifyConnected(serv *ChatServer, user shared.User) {
	notificationMessage := shared.Message{
		UserId:      serv.user.Id,
		DisplayName: serv.user.DisplayName,
		Content:     fmt.Sprintf("User %s(%s) has joined", user.DisplayName, user.Id),
	}
	sendToAll(serv, notificationMessage)
}

func (serv *ChatServer) run() {
	for {
		select {
		case newConn := <-serv.events.newConnections:
			fmt.Println("New connection found")
			go handleConnect(newConn, serv.nextUserId, serv.events)
			serv.nextUserId = serv.nextUserId.Next()
		case established := <-serv.events.establishedConnections:
			fmt.Printf("%s[%s] connected\n", established.user.DisplayName, established.user.Id)
			serv.connectedUsers[established.user.Id] = established
			notifyConnected(serv, established.user)
			go established.run()
		case disconnectedUserId := <-serv.events.disconnectedUsers:
			if userConn, ok := serv.connectedUsers[disconnectedUserId]; ok {
				userInfo := userConn.user
				fmt.Printf("%s[%s] disconnected\n", userInfo.DisplayName, userInfo.Id)
				delete(serv.connectedUsers, disconnectedUserId)
			}
		case message := <-serv.events.messages:
			fmt.Printf("%s[%s]: %s\n", message.UserId, message.DisplayName, message.Content)
			sendToAll(serv, message)
		}
	}
}

func main() {
	serverUserId := shared.NewUserId()
	serv := ChatServer{
		connectedUsers: map[shared.UserId]ClientConnection{},
		events: ServerEvents{
			establishedConnections: make(chan ClientConnection),
			newConnections:         make(chan net.Conn),
			disconnectedUsers:      make(chan shared.UserId),
			messages:               make(chan shared.Message),
		},
		user: shared.User{
			Id:          serverUserId,
			DisplayName: "GoChat Server",
		},
		nextUserId: serverUserId.Next(),
	}
	listener, err := net.Listen("tcp", ":5556")

	if err != nil {
		fmt.Println("Could not listen: %s", err.Error())
		os.Exit(1)
	}
	defer listener.Close()

	fmt.Println("Server Listener Started")

	go serv.run()

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Failed to accept connection")
			continue
		}

		serv.events.newConnections <- conn
	}
}
