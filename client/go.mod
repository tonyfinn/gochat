module tonyfinn.com/gochat/client

go 1.17

replace tonyfinn.com/gochat/shared => ../shared

require (
	github.com/rthornton128/goncurses v0.0.0-20210302221415-1355ee05acae
	tonyfinn.com/gochat/shared v0.0.0-00010101000000-000000000000
)
