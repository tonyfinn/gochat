package main

import (
	"fmt"
	"net/textproto"
	"time"

	"github.com/rthornton128/goncurses"
	"tonyfinn.com/gochat/shared"
)

const asciiBackspace = 127

func intMax(x, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}

type CursesInterface struct {
	logWindow   *goncurses.Window
	inputWindow *goncurses.Window
	inputBuf    string
}

func setupInterface() (ui CursesInterface, err error) {
	screen, err := goncurses.Init()
	if err != nil {
		return
	}
	goncurses.CBreak(true)
	goncurses.Echo(false)

	height, width := screen.MaxYX()
	screen.Resize(height-1, width)
	screen.ScrollOk(true)
	screen.Timeout(0)
	screen.Println("Enter display name")

	inputWindow, _ := goncurses.NewWindow(1, width, height-1, 0)
	inputWindow.ScrollOk(true)

	inputBuf := ""

	return CursesInterface{
		logWindow:   screen,
		inputWindow: inputWindow,
		inputBuf:    inputBuf,
	}, nil
}

func displayLine(ui *CursesInterface, line string) {
	ui.logWindow.Println(line)
	ui.inputWindow.Move(0, 0)
}

func deleteChar(ui *CursesInterface) {
	endIndex := intMax(0, len(ui.inputBuf)-1)
	ui.inputBuf = ui.inputBuf[:intMax(0, endIndex-1)]
	ui.inputWindow.DelChar()
	moveCursorToInputEnd(ui)
}

func moveCursorToInputEnd(ui *CursesInterface) {
	xEnd := intMax(len(ui.inputBuf)-1, 0)
	ui.inputWindow.Move(0, xEnd)
}

func echoCharacter(ui *CursesInterface, inputch string) {
	ui.inputWindow.Printf(inputch)
	ui.inputBuf += inputch
}

func handleUserInput(ui *CursesInterface, messagesToServer chan string) bool {
	input := ui.logWindow.GetChar()
	if input == goncurses.KEY_RETURN || input == goncurses.KEY_ENTER {
		ui.inputWindow.Erase()
		if ui.inputBuf == "/quit" {
			return false
		}
		messagesToServer <- ui.inputBuf
		ui.inputBuf = ""
		moveCursorToInputEnd(ui)
	} else if input < 0x20 {
		// Control character do nothing
	} else if input == goncurses.KEY_BACKSPACE || input == asciiBackspace {
		deleteChar(ui)
	} else if input != 0 {
		inputch := goncurses.KeyString(input)
		echoCharacter(ui, inputch)
	}
	return true
}

func handleExternalInput(ui *CursesInterface, messagesFromServer chan string) {
	queueEmpty := false
	for queueEmpty == false {
		select {
		case input := <-messagesFromServer:
			displayLine(ui, input)
			moveCursorToInputEnd(ui)
			ui.inputWindow.Refresh()
		default:
			queueEmpty = true
		}
	}
}

func uiLoop(ui *CursesInterface, messagesFromServer chan string, messagesToServer chan string) bool {
	shouldContinue := handleUserInput(ui, messagesToServer)
	ui.inputWindow.Refresh()
	handleExternalInput(ui, messagesFromServer)
	return shouldContinue
}

func networkLoop(messagesFromServer chan string, messagesToServer chan string) error {
	conn, err := textproto.Dial("tcp", "localhost:5556")
	if err != nil {
		return err
	}
	go (func() {
		for {
			line, err := conn.ReadLine()
			if err != nil {
				messagesFromServer <- fmt.Sprintf("Connection closed: %s", err)
				break
			} else {
				messagesFromServer <- fmt.Sprintf(line)
			}
		}
	})()

	for {
		msg := <-messagesToServer
		err = conn.PrintfLine(msg)
		if err != nil {
			break
		}
	}
	return nil
}

func main() {
	ui, err := setupInterface()
	if err != nil {
		fmt.Println("Failed to init curses ui")
		return
	}
	defer goncurses.End()
	messagesFromServer := make(chan string, 10)
	messagesToServer := make(chan string, 10)
	go networkLoop(messagesFromServer, messagesToServer)
	for {
		shouldContinue := uiLoop(&ui, messagesFromServer, messagesToServer)
		if !shouldContinue {
			break
		}
		time.Sleep(300_000)
	}
}
