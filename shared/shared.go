package shared

import (
	"fmt"
)

type UserId uint64
type MessageId uint64
type ChannelId uint64

func NewUserId() UserId {
	return 0
}

func (uid UserId) Next() UserId {
	return uid + 1
}

func (uid UserId) String() string {
	return fmt.Sprintf("UserId(%d)", int(uid))
}

type Message struct {
	UserId      UserId `json:"uid"`
	DisplayName string `json:"displayName"`
	Content     string `json:"content"`
}

func (m Message) String() string {
	return fmt.Sprintf("Message(Uid=%d, DisplayName=%s, Content=%s)", m.UserId, m.DisplayName, m.Content)
}

type User struct {
	Id          UserId `json:"id"`
	DisplayName string `json:"displayName"`
}

type Channel struct {
	Id          ChannelId
	Users       []UserId
	Description string
}
